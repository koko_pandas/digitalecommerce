'use strict';

angular.module('EComApp', [ 'ngAnimate', 'ui.router', 'restangular'
                     , 'cgNotify', 'ngAutocomplete', 'ui.bootstrap', 
                     'mwl.calendar', 'angular-loading-bar', 'ui.bootstrap.datetimepicker']).config(
		function($stateProvider, $urlRouterProvider, RestangularProvider, 
				$httpProvider) {
			$urlRouterProvider.otherwise("/login");
			debugger;
			$stateProvider.state('home', {
				url : "/login",
				templateUrl : "static/views/auth.html",
				controller : 'LoginCtrl'
			});
			
			$stateProvider.state('register', {
				url : "/register",
				templateUrl : "static/views/register.html",
				controller : 'LoginCtrl'
			});
			
			$stateProvider.state('forgotPwd', {
				url : "/forgotPwd",
				templateUrl : "static/views/pwd/forgotpassword.html",
				controller : 'PwdCtrl'
			});
			
			$stateProvider.state('verify', {
				url : "/verify",
				templateUrl : "static/views/verify.html",
				controller : 'LoginCtrl'
			});
			
			$stateProvider.state('verifyuser', {
				url : "/activate",
				templateUrl : "static/views/verifyuser.html",
				controller : 'VerifyCtrl'
			});
			
			$stateProvider.state('userhome', {
				url : "/userhome",
				templateUrl : "static/views/homecontent.html",
				controller : 'HomeCtrl',
			});
			
			$stateProvider.state('product', {
				url : "/addproduct",
				templateUrl : "static/views/addproduct.html",
				controller : 'ProductCtrl',
			});
			
			$stateProvider.state('productList', {
				url : "/viewproducts",
				templateUrl : "static/views/viewproducts.html",
				controller : 'ProductCtrl',
			});
			
			RestangularProvider.setBaseUrl('/ecommerce');
			RestangularProvider.setRestangularFields({
				id : 'id'
			});
			RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
			});


		}).run(function($rootScope, $state, $stateParams, notify, AuthService, AUTH_EVENTS) {
	$rootScope.$state = $state;
	$rootScope.$stateParams = $stateParams;
	$rootScope.notify = notify;
});
