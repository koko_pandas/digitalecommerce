//Fetches Model and minds to view
//Can use services to make http calls.

// Inspired from https://medium.com/opinionated-angularjs/techniques-for-authentication-in-angularjs-applications-7bbf0346acec

(function() {
	'use strict';
	var app = angular.module('EComApp').controller('LoginCtrl', function($rootScope, $scope, $state, $http, notify, AUTH_EVENTS, AuthService, Session) {
		debugger;
		$scope.loginUser = function(userVO) {
			debugger;
			var body = "username=" + userVO.username + "&password=" + userVO.password;
			$http({
				url :"/ecommerce/user/login",
				method : 'POST',
				'headers' : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				},
				data : body
			}).then(function(dataResponse) {
				debugger;
				if (dataResponse.data.status == "success") {
					notify('Login Successfull');
					$rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
					$scope.setCurrentUser(userVO);
					// FIXME
					Session.create("sessionId", 1, "SYSTEM_ADMIN");
					$state.transitionTo("userhome");
				} else {
					// TODO - notify Warning color ??
					$rootScope.$broadcast(AUTH_EVENTS.loginFailed);
					notify('Username or Password Invalid or Activate Your Account.');
				}
			});

		};

		$scope.registerUser = function(registerVO) {
			debugger;
			$http({
				url : "/ecommerce/user/register",
				method : 'POST',
				dataType : 'json',
				'headers' : {
					'Content-Type' : 'application/json'
				},
				data : registerVO
			}).success(function(data, status, headers, config) {
					$rootScope.emailId = registerVO.emailId;

					$state.transitionTo("verify");
			}).error(function(data, status, headers, config) {
				notify('Email ' + registerVO.emailId + ' already exists.');			    
			});
		};

		$scope.verifyUser = function(verifyVO) {
			debugger;
			verifyVO.emailId = $rootScope.emailId; // FIXME not a good idea to
													// maintain email id in root
													// scope
			$http({
				url : "/ecommerce/user/verify",
				method : 'POST',
				dataType : 'json',
				'headers' : {
					'Content-Type' : 'application/json'
				},
				data : verifyVO
			}).then(function(dataResponse) {
				debugger;
				if (dataResponse.data.status == "success") {
					$state.transitionTo("home");
				} else if (dataResponse.data.status == "invalid") {
					notify('Oops ! In valid activation code. Make sure activation code is entered is correct.');
				}
			});
		};

	});
})();