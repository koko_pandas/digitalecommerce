(function() {
	'use strict';
	var app = angular.module('EComApp').controller('VerifyCtrl', function($rootScope, $scope, $state, $http, notify, AUTH_EVENTS, AuthService, Session) {
		debugger;
		
		$scope.verifyUser = function(verifyVO) {
			debugger;
			$http({
				url : "/user/verify",
				method : 'POST',
				dataType : 'json',
				'headers' : {
					'Content-Type' : 'application/json'
				},
				data : verifyVO
			}).then(function(dataResponse) {
				debugger;
				if (dataResponse.data.status == "success") {
					notify('Your account is activated successfully.');
					$state.transitionTo("home");
				} else if (dataResponse.data.status == "invalid") {
					notify('Oops ! In valid activation code. Make sure activation code is entered is correct.');
				}
			});
		};
		
		});
})();