angular.module('EComApp').factory('AuthService', function($http, Session) {
	var authService = {};

	authService.isAuthenticated = function() {
		debugger;
		return Session.userId != "";
	};

	authService.isAuthorized = function(authorizedRoles) {
		debugger;
		if (!angular.isArray(authorizedRoles)) {
			authorizedRoles = [ authorizedRoles ];
		}
		return (authService.isAuthenticated() && authorizedRoles.indexOf(Session.userRole) !== -1);
	};

	return authService;
});