//Fetches Model and minds to view
//Can use services to make http calls.

// Inspired from https://medium.com/opinionated-angularjs/techniques-for-authentication-in-angularjs-applications-7bbf0346acec

(function() {
	'use strict';
	var app = angular.module('EComApp').controller('ProductCtrl', function( $scope, $state, $http, notify) {
		debugger;
		$scope.ProductVO={};
		$scope.saveProduct = function(productVO){
			debugger;
			$http({
				url:"/ecommerce/products",
				method : 'POST',
				dataType : 'json',
				'headers' : {
					'Content-Type' : 'application/json'
				},
				data : productVO
			}).success(function(data, status, headers){
				debugger;
				if(data.status=="success"){
					notify('Product Succesfully added');
					$state.transitionTo("productList");
				}
				else{
					notify('Problem occured while saving, try again!')
				}
			}).error(function(data,status){
				notify('Problem occured while saving, try again!')
			});
		}
	
	
	$scope.products = [];
	
	$scope.product = [];
	$scope.userId = '';
	
	$scope.getProducts = function() {
		debugger;
		$http({
			url : "/ecommerce/products",
			method : 'GET',
			dataType : 'json'
		}).success(function(data) {
			$scope.products = data;
		});
	};
	$scope.getProducts();
	
	$scope.deleteProduct = function(id){
		debugger;
		$http({
			url:"/ecommerce/products/"+id,
			method : 'DELETE'
		}).success(function(data){
			notify('Product Deleted!');
			$scope.getProducts();
		});
	}
	
	});
})();