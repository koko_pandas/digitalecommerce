angular.module('EComApp').controller('ApplicationCtrl', function($scope, $state, USER_ROLES, AuthService, Session) {
	$scope.currentUser = null;
	$scope.emailId = '';
	$scope.userRoles = USER_ROLES;
	$scope.isAuthorized = AuthService.isAuthorized;

	$scope.setCurrentUser = function(user) {
		$scope.currentUser = user;
	};
	
	$scope.logout = function() {
		debugger;
		// TODO - > hit server to logout and then do below
		Session.destroy();
		$scope.setCurrentUser(null);
		$state.transitionTo("home");
	};
	
});
