package com.sareeta.ecommerce.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sareeta.ecommerce.dao.ProductRepository;
import com.sareeta.ecommerce.dto.ProductDTO;
import com.sareeta.ecommerce.entity.Product;
import com.sareeta.ecommerce.service.ProductService;
import com.sareeta.ecommerce.util.ResponseStatus;

@Service
public class ProductServiceImpl implements ProductService{
	
	@Autowired
	private ProductRepository productRepository;

	@Override
	@Transactional
	public ResponseStatus saveProduct(ProductDTO productDTO) {
		ResponseStatus rs = new ResponseStatus();
		Product prod = new Product();
		prod.setProductName(productDTO.getProductName());
		prod.setAvailableQuantity(productDTO.getAvailableQuantity());
		prod.setCategory(productDTO.getCategory());
		prod.setPrice(productDTO.getPrice());
		productRepository.save(prod);
		rs.setStatus("success");
		return rs;
	}

	@Override
	public List<ProductDTO> getProducts() {
		
		List<Product> allProducts = productRepository.findAll();
		List<ProductDTO> prodDTO= new ArrayList<ProductDTO>();
		for(Product obj: allProducts){
			ProductDTO prod = new ProductDTO();
			prod.setAvailableQuantity(obj.getAvailableQuantity());
			prod.setCategory(obj.getCategory());
			prod.setPrice(obj.getPrice());
			prod.setProductName(obj.getProductName());
			prod.setId(obj.getId());
			prodDTO.add(prod);
		}
		return prodDTO;
	}

	@Override
	public void deleteProducts(long id) {
		productRepository.delete(id);
	}

}
