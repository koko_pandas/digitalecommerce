/**
 * 
 */
package com.sareeta.ecommerce.service;

import java.util.List;

import com.sareeta.ecommerce.dto.ProductDTO;
import com.sareeta.ecommerce.util.ResponseStatus;

/**
 * @author sareetasimanchalapanda
 *
 */

public interface ProductService {

	ResponseStatus saveProduct(ProductDTO productDTO);

	List<ProductDTO> getProducts();

	void deleteProducts(long id);
}
