package com.sareeta.ecommerce.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.sareeta.ecommerce.dao.UserRepository;
import com.sareeta.ecommerce.dao.spec.UserSpecification;
import com.sareeta.ecommerce.dto.RegisterDTO;
import com.sareeta.ecommerce.entity.CustomUserDetails;
import com.sareeta.ecommerce.entity.User;
import com.sareeta.ecommerce.service.UserService;
import com.sareeta.ecommerce.util.ResponseStatus;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		User userByEmail = userRepository.findOne(UserSpecification
				.getUserByEmail(username));
		if (userByEmail != null) {
			List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			String password = userByEmail.getPassword();
			String userStatus = userByEmail.getStatus().toString();
			if (("ACTIVE").equalsIgnoreCase(userStatus)) {
				authorities.add(new SimpleGrantedAuthority("ROLE_" + "USER"));
				UserDetails user = new CustomUserDetails(
						userByEmail.getUserName(), password, authorities,
						userByEmail.getId(), null);
				return user;
			}
		}
		return null;

	}

	@Override
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;

	}

	@Override
	public ResponseStatus register(RegisterDTO registerDTO) {
		String randomString = RandomStringUtils.randomAlphanumeric(5);
		ResponseStatus rs = new ResponseStatus();
		User user = new User();
		user.setEmail(registerDTO.getEmailId());
		user.setUserName(registerDTO.getFirstName());
		user.setPassword(randomString);
		user.setStatus("ACTIVE");
		userRepository.save(user);
		rs.setStatus("success");
		return rs;
	}

}
