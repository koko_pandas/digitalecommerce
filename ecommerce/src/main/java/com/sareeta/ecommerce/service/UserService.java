package com.sareeta.ecommerce.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.sareeta.ecommerce.dao.UserRepository;
import com.sareeta.ecommerce.dto.RegisterDTO;
import com.sareeta.ecommerce.util.ResponseStatus;


public interface UserService extends UserDetailsService{

	void setUserRepository(UserRepository userRepository);

	ResponseStatus register(RegisterDTO registerDTO);

	
	

}
