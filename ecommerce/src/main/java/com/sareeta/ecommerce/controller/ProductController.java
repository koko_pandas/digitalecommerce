package com.sareeta.ecommerce.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sareeta.ecommerce.dto.ProductDTO;
import com.sareeta.ecommerce.service.ProductService;
import com.sareeta.ecommerce.util.ResponseStatus;

@Controller
@RequestMapping(value = "/products")
public class ProductController {

	@Autowired
	private ProductService productService;

	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public ResponseStatus addProduct(@RequestBody ProductDTO productDTO) {
		return productService.saveProduct(productDTO);
	}

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public List<ProductDTO> getAllProduct() {
		return productService.getProducts();
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "{id}")
	public void deleteProduct(@PathVariable long id) {
		productService.deleteProducts(id);
	}

}
