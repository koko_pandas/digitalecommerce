package com.sareeta.ecommerce.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sareeta.ecommerce.dto.RegisterDTO;
import com.sareeta.ecommerce.service.UserService;
import com.sareeta.ecommerce.util.ResponseStatus;

@Controller
@RequestMapping(value = "/user")
public class UserController {
	@Autowired
	UserService userService;

	@RequestMapping(value = "/login/success")
	@ResponseBody
	public ResponseStatus success() {
		ResponseStatus rs = new ResponseStatus();
		rs.setStatus("success");
		return rs;
	}

	@RequestMapping(value = "/login/error")
	@ResponseBody
	public ResponseStatus returnError() {
		ResponseStatus rs = new ResponseStatus();
		rs.setStatus("error");
		return rs;
	}

	@ResponseBody
	@RequestMapping(value = "/register", method = RequestMethod.POST, produces = "application/json")
	public ResponseStatus registerUser(@RequestBody RegisterDTO rDto) {
		return userService.register(rDto);
	}

}
