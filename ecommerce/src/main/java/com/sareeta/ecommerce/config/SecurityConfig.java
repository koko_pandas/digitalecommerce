package com.sareeta.ecommerce.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.userdetails.DaoAuthenticationConfigurer;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.sareeta.ecommerce.dao.UserRepository;
import com.sareeta.ecommerce.service.UserService;
import com.sareeta.ecommerce.service.impl.UserServiceImpl;

@EnableWebSecurity
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	private UserService userService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private DataSource dataSource;

	protected void configure(AuthenticationManagerBuilder registry)
			throws Exception {
		userService = new UserServiceImpl();
		userService.setUserRepository(userRepository);

		DaoAuthenticationConfigurer<AuthenticationManagerBuilder, UserService> daoAuthenticationConfigurer = registry
				.userDetailsService(userService);
		registry.jdbcAuthentication().dataSource(dataSource);

	}

	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/resources/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.csrf()
				.disable()
				.authorizeRequests()
				.antMatchers("/**").permitAll().anyRequest().authenticated()
				.and().formLogin().loginPage("/")
				.loginProcessingUrl("/user/login")
				.defaultSuccessUrl("/user/login/success")
				.failureUrl("/user/login/error").permitAll().and().logout()
				.logoutUrl("/logout").logoutSuccessUrl("/");
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		return bCryptPasswordEncoder;
	}
}
