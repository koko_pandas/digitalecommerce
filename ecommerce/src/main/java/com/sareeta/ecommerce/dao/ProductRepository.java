package com.sareeta.ecommerce.dao;

import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.sareeta.ecommerce.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Serializable>{

}
