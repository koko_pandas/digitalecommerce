package com.sareeta.ecommerce.dao.spec;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.sareeta.ecommerce.entity.User;

public class UserSpecification {

	public static Specification<User> getUserByEmail(final String email){
	return new Specification<User>() {
		
		@Override
		public Predicate toPredicate(Root<User> root, CriteriaQuery<?> criteriaQuery,
				CriteriaBuilder criteriaBuilder) {
			Predicate predicate = criteriaBuilder.equal(root.get("email"), email);
			return predicate;
		}
		
	};
}
	
	
	
}
